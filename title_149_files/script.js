function message(text){
	$('#alert-message').hide();
	$('#alert-message p').text(text);
	$('#alert-message').fadeIn('slow');
	setTimeout(function(){
		$('#alert-message').fadeOut('slow');
	}, 3000);
}
$('document').ready(function(){
	var d = $('#images50');
	var url = d.attr('data-url');
	
	d.find('.partner-picture').each(function(){
		var th = $(this);
		var src = th.find('img').attr('src');
		th.find('.image-zoom-in').colorbox({href: src});
		th.find('.image-remove').click(function(){
			$.ajax({
			  url: url,
			  method: 'post',
			  data: {url: [src]},
			  success: function(){
				  th.addClass('disable');
				  message('Импорт картинки прошел успешно');
			  }
			});
			  
		});
	});
	
	var data = []
	d.find('.text-center button').click(function(){
		d.find('.partner-picture').each(function(){
			data.push($(this).find('img').attr('src'));
		});
		$.ajax({
			url: url,
			method: 'post',
			data: {url: data},
			success: function(){
				d.find('.partner-picture').addClass('disable');
				message('Импорт картинок прошел успешно');
			}
		});
	});
})